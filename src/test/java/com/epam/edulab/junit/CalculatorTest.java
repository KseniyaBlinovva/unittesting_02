package com.epam.edulab.junit;

import static org.hamcrest.CoreMatchers.*;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;

public class CalculatorTest {

    @Test
    public void addTest(){
        Double result = Calculator.calculate("10 +2");
        assertThat(result, is(12d));
    }

    @Test
    public void subTest() {
        double result = Calculator.calculate("10 - 2");
        assertThat(result,is(8d));

    }

    @Test
    public void multTest() {
        double result = Calculator.calculate("2 * 3");
        assertThat(result,is(6d));
    }

    @Test
    public void divTest() {
        double result = Calculator.calculate("6 / 2");
        assertThat(result,is(3d));
    }
    @Test(expected = IllegalArgumentException.class)
    public void divZeroTest() {
        double result = Calculator.calculate("12 / 0");
        assertThat(result, is(Double.POSITIVE_INFINITY));
    }
    @Test
    public void invalidInput(){

    }

}